# Instalación
Requiere MongoDB. La configuración de mongoose está en src/models/Users.js

Dentro del directorio

npm install

# Uso
Express se ejecuta en el puerto 8000

Al acceder al raiz te redirige a la página de login.

Antes es necesario registrar un usuario para poder autenticarse.

http://localhost:8000/register

Una vez autenticado el usuario en

http://localhost:8000/login

Te redirige al end-point con el listado de gasolineras (el listado es referente a la provincia indicada en el registro). 
Este endpoint muestra los datos que le devuelve una ruta protegida mediante el token de usuario que hace la petición a la API externa.
Si el usuario no está logeado no se muestran datos.

