const express = require('express');
const path = require('path');
const bodyParser = require('body-parser');
const session = require('express-session');
const cors = require('cors');
const mongoose = require('mongoose');
const errorHandler = require('errorhandler');
const hbs = require('express-handlebars');
const fetch = require('node-fetch');
const { exec } = require('child_process');

//Configure mongoose's promise to global promise
mongoose.promise = global.Promise;

//Configure isProduction variable
const isProduction = process.env.NODE_ENV === 'production';

//Initiate our app
const app = express();

//Configure our app
app.use(cors()); //acceso cors
app.use(require('morgan')('dev')); //morgan es un logger
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(express.static(path.join(__dirname, '../public')));
app.use(session({ secret: 'alfa-streaming', cookie: { maxAge: 60000 }, resave: false, saveUninitialized: false }));

if(!isProduction) {
    app.use(errorHandler());
}

//Configure Mongoose
mongoose.connect('mongodb://localhost:27017/carburantes');
mongoose.set('debug', true);


/* Models y rutas */
require('./models/Users');
require('./config/passport');
const auth = require('./routes/auth');
app.use(require('./routes'));

// Defino el motor de plantillas
app.engine('hbs', hbs({
    defaultLayout: 'default',
    extname: '.hbs'
}));
app.set('view engine', 'hbs');

app.listen(8000, () => console.log('Server running on http://localhost:8000/'));

// Defino los End-points de la aplicación
app.get('/login', auth.optional, (req, res) => {
    res.render('login');
});

app.get('/register', auth.optional, (req, res) => {
    res.render('register');
});

app.get('/listadoGasolineras/:id', auth.optional, (req, res) => {
    let id = req.params.id;
    res.render('listado', { id: id });
});

app.get('/precioCarburantes/:id', auth.required, (req, res) => {
    let id = req.params.id;
    let command = 'curl https://sedeaplicaciones.minetur.gob.es/ServiciosRESTCarburantes/PreciosCarburantes/EstacionesTerrestres/FiltroProvincia/' + id;
    exec(command , (err, stdout, stderr) => {
        if (err) {
            res.end("ERROR");
            return;
        }
        //console.log(`stdout: ${stdout}`);
        // console.log(`stderr: ${stderr}`);
        let datos = JSON.parse(stdout);
        res.send(datos.ListaEESSPrecio);
    });
});

app.get('/', auth.optional, function(req, res) {
    res.render('home');
});

app.get('/logout', auth.optional, function(req, res) {
    res.redirect('/login');
});
